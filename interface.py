from tkinter import *
from chiffrage import chiffrage
from dechiffrage import dechiffrage
frame = Tk()
frame.configure(background = "#191919")
frame.title("Chiffrage de Cesar")
icone = PhotoImage(file='icone.gif')
frame.iconphoto(False,icone)

def doChiffrage():
    if (decalage.get() < 0):
        return
    chaine = ChiffrageTxtBox.get("0.0", END) # Recupère le contenu du champs de saisie.
    chaine = chiffrage(chaine,decalage.get(),circulaire.get() )
    DechiffrageTxtBox.delete("0.0",END)
    DechiffrageTxtBox.insert("0.0", chaine)

def doDechiffrage():
    if (decalage.get() < 0):
        return
    chaine = DechiffrageTxtBox.get("0.0", END) # Recupère le contenu du champs de saisie.
    chaine = dechiffrage(chaine,decalage.get(),circulaire.get())
    ChiffrageTxtBox.delete("0.0",END)
    ChiffrageTxtBox.insert("0.0", chaine)



titreLbl = Label(frame, text="Chiffrage de Cesar",bg="#191919", fg="white", width="20", font="-size 22")
titreLbl.grid(column = 0, row = 0, padx="20", columnspan=2)

ChiffrageTxtBox = Text(frame, wrap='word')
ChiffrageTxtBox.grid(column = 0, row = 1, padx="20")

DechiffrageTxtBox = Text(frame, wrap='word')
DechiffrageTxtBox.grid(column = 1, row = 1)

boutonChiffrage = Button(frame, text='Chiffrer', width="20", font="-size 13", command=doChiffrage)
boutonChiffrage.grid(column = 0,row = 3, pady="20")

boutonDechiffrage = Button(frame, text='Dechiffrer', width="20", font="-size 13", command=doDechiffrage)
boutonDechiffrage.grid(column = 1,row = 3, pady="20")

decalage = IntVar()
decalageLbl = Label(frame, text="Décalage :",bg="#191919", fg="white", font="-size 12")
decalageLbl.grid(column = 0, row = 0, sticky="w", padx="225")
SaisieDecalage = Entry(frame, textvariable = decalage, width="5")
SaisieDecalage.grid(column = 0, row = 0)
circulaire = BooleanVar()
circulaire.set("False")
checkboxCirculaire = Checkbutton(frame, text="Mode Circulaire",
                                 state=ACTIVE, variable=circulaire,
                                 fg = "white", bg = "#191919",
                                 activebackground="#191919",
                                 activeforeground="white",
                                 selectcolor = "#191919",)


checkboxCirculaire.grid(column = 0, row = 0, sticky="w")


frame.mainloop()
