import unittest
from dechiffrage import dechiffrage

# string dechiffrage(string chaine, int decalage, boolean Circulaire)
# Dans ces tests, Circulaire sera faux.
class TestDechiffrageNonCirculaireFonction(unittest.TestCase):
    def test_dechiffrage(self):
        self.assertEqual(dechiffrage("ijkl",8,False),"abcd")
        self.assertEqual(dechiffrage("Pmttw _wztl",8,False),"Hello World")
        self.assertEqual(dechiffrage("defg",3,False),"abcd")
        self.assertEqual(dechiffrage("Nkrru ]uxrj",6,False),"Hello World")

# string dechiffrage(string chaine, int decalage, boolean Circulaire)
# Dans ces tests, Circulaire sera vrai.
class TestDechiffrageCirculaireFonction(unittest.TestCase):
    def test_dechiffrage(self):
        self.assertEqual(dechiffrage("abc",3,True),"xyz")
        self.assertEqual(dechiffrage("IJK",8,True),"ABC")
        self.assertEqual(dechiffrage("YES",10,True),"OUI")
        self.assertEqual(dechiffrage("DEFGHIJKLMNOPQRSTUVWXYZABC",3,True),
                                     "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        self.assertEqual(dechiffrage("defghijklmnopqrstuvwxyzabc",3,True),
                                     "abcdefghijklmnopqrstuvwxyz")
        self.assertEqual(dechiffrage("Uftu 123_&-é",1,True),"Test 123_&-é")
