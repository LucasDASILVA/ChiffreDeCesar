import unittest
from chiffrage import chiffrage

# string chiffrage(string chaine, int decalage, boolean Circulaire)
# Dans ces tests, Circulaire sera faux.
class TestChiffrageNonCirculaireFonction(unittest.TestCase):
    def test_chiffrage(self):
        self.assertEqual(chiffrage("abcd",8,False),"ijkl")
        self.assertEqual(chiffrage("Hello World",8,False),"Pmttw _wztl")
        self.assertEqual(chiffrage("abcd",3,False),"defg")
        self.assertEqual(chiffrage("abcd",8,False),"ijkl")
        self.assertEqual(chiffrage("Hello World",6,False),"Nkrru ]uxrj")

# string chiffrage(string chaine, int decalage, boolean Circulaire)
# Dans ces tests, Circulaire sera vrai.
class TestChiffrageCirculaireFonction(unittest.TestCase):
    def test_chiffrage(self):
        self.assertEqual(chiffrage("xyz",3,True),"abc")
        self.assertEqual(chiffrage("ABC",8,True),"IJK")
        self.assertEqual(chiffrage("OUI",10,True),"YES")
        self.assertEqual(chiffrage("ABCDEFGHIJKLMNOPQRSTUVWXYZ",3,True),
                                   "DEFGHIJKLMNOPQRSTUVWXYZABC")
        self.assertEqual(chiffrage("abcdefghijklmnopqrstuvwxyz",3,True),
                                   "defghijklmnopqrstuvwxyzabc")
        self.assertEqual(chiffrage("Test 123_&-é",1,True),"Uftu 123_&-é")
