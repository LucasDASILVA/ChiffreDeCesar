import unittest
from decomposition import decomposition

class TestDecompositionFonction(unittest.TestCase):
    def test_decomposition(self):
        self.assertEqual(decomposition("abc"),['a','b','c'])
        self.assertEqual(decomposition("Hello World !"),['H','e','l','l','o',' ','W','o','r','l','d',' ','!'])