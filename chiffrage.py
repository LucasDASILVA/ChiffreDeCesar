# Ce fichier contient 3 fonctions :
# 
# string chiffrage(string chaineCryptee, int decalage, boolean circulaire) 
# --> Cette fonction est appelée lorsque l'utilisateur appuie sur le bouton chiffrage.
# string chiffrageNonCirculaire(string chaineCryptee,int decalage)
# --> Appelée dans chiffrage(...), elle chiffre la chaine avec le décalage lorsque le mode circulaire est désactivé.
# string chiffrageCirculaire(string chaineCryptee, int decalage)
# --> Appelée dans chiffrage(...), elle chiffre la chaine avec le décalage lorsque le mode circulaire est activé. 

from decomposition import decomposition

#=================================================

# Détermine si le chiffrement doit être circulaire ou non.
def chiffrage(chaineClaire,decalage,circulaire):
    if(circulaire == False):
        chaineChiffree = chiffrageNonCirculaire(chaineClaire,decalage)
    else:
        chaineChiffree = chiffrageCirculaire(chaineClaire,decalage)
    return chaineChiffree

#=================================================

def chiffrageNonCirculaire(chaineClaire, decalage):
    chaineChiffree = ''
    liste = decomposition(chaineClaire) # Décompose une chaine en liste (ex: la chaine "abcd" devient la liste ['a','b','c','d'])
    longueurListe = len(liste)
    indice = 0
  
    while(indice != longueurListe):
        codeCar = ord(liste[indice])+decalage
        if( (codeCar != (32+decalage)) & (codeCar!=(10+decalage)) ): # Vérifie que le caractère n'est pas un espace ni un retour ligne.
            liste[indice] = chr(codeCar)
        indice = indice + 1
    chaineChiffree = "".join(liste) # Concatène tous les caractères de la liste pour former une chaine (ex: ['H','e','y'] devient la chaine "Hey")
    return chaineChiffree

#=================================================
# Le chiffrement circulaire ne s'applique qu'aux caractères issus de l'alphabet latin.
def chiffrageCirculaire(chaineClaire, decalage):
    chaineChiffree = ''
    liste = decomposition(chaineClaire) # Décompose une chaine en liste (ex: la chaine "abcd" devient la liste ['a','b','c','d'])
    longueurListe = len(liste)
    indice = 0
 
    while(indice != longueurListe):
        codeCar = ord(liste[indice])
        if( (codeCar != (32+decalage)) & (codeCar!=(10+decalage)) ): # Vérifie que le caractère n'est pas un espace ni un retour ligne.

            if ((codeCar >= 65) & (codeCar <= 90 )): # Vérifie que le caractère soit dans l'alphabet MAJUSCULE.
                if (codeCar+decalage > 90): # Vérifie si un retour circulaire est nécessaire. 
                    codeCar = codeCar+decalage-26
                else:
                    codeCar = codeCar+decalage
            if ((codeCar >= 97) & (codeCar <= 122 )): # Vérifie que le caractère soit dans l'alphabet MINUSCULE.
                if (codeCar+decalage > 122): # Vérifie si un retour circulaire est nécessaire. 
                    codeCar = codeCar+decalage-26
                else:
                    codeCar = codeCar+decalage

            liste[indice] = chr(codeCar)
        indice = indice + 1
    chaineChiffree = "".join(liste) # Concatène tous les caractères de la liste pour former une chaine (ex: ['H','e','y'] devient la chaine "Hey")
    return chaineChiffree
#=================================================