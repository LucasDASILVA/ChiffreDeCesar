# Ce fichier contient 3 fonctions :
# 
# string dechiffrage(string chaineCryptee, int decalage, boolean circulaire) 
# --> Cette fonction est appelée lorsque l'utilisateur appuie sur le bouton déchiffrage.
# string dechiffrageNonCirculaire(string chaineCryptee,int decalage)
# --> Appelée dans dechiffrage(...), elle déchiffre la chaine avec le décalage lorsque le mode circulaire est désactivé.
# string dechiffrageCirculaire(string chaineCryptee, int decalage)
# --> Appelée dans dechiffrage(...), elle déchiffre la chaine avec le décalage lorsque le mode circulaire est activé. 

from decomposition import decomposition

 #=================================================

# Détermine si le dechiffrement doit être circulaire ou non.
def dechiffrage(chaineCryptee,decalage,circulaire):
    if(circulaire == False):
        chainedeChiffree = dechiffrageNonCirculaire(chaineCryptee,decalage)
    else:
        chainedeChiffree = dechiffrageCirculaire(chaineCryptee,decalage)
    return chainedeChiffree

 #=================================================

def dechiffrageNonCirculaire(chaineCryptee, decalage):
    chainedeChiffree = ''
    liste = decomposition(chaineCryptee) # Décompose une chaine en liste (ex: la chaine "abcd" devient la liste ['a','b','c','d'])
    longueurListe = len(liste)
    indice = 0

    #Prend le code ASCII de chaque caractères dans la liste et lui enlève le nombre de chiffre à décaler
    while(indice != longueurListe):
        codeCar = ord(liste[indice])-decalage
        if( (codeCar != (32-decalage) ) & (codeCar!=(10-decalage) ) ): # Vérifie que le caractère n'est pas un espace ou un retour ligne.
            liste[indice] = chr(codeCar)
        indice = indice + 1
    chainedeChiffree = "".join(liste) # Concatène tous les caractères de la liste pour former une chaine (ex: ['H','e','y'] devient la chaine "Hey")
    return chainedeChiffree

    #=================================================
# Le dechiffrement circulaire ne s'applique qu'aux caractères issus de l'alphabet latin.
def dechiffrageCirculaire(chaineCryptee,decalage):
    chainedeChiffree = ''
    liste = decomposition(chaineCryptee) # Décompose une chaine en liste (ex: la chaine "abcd" devient la liste ['a','b','c','d'])
    longueurListe = len(liste)
    indice = 0

    while(indice != longueurListe):
        codeCar = ord(liste[indice])
        if( (codeCar >= 65) & (codeCar <= 90) ): # Vérifie que le caractère soit dans l'alphabet MAJUSCULE.
            if(codeCar-decalage < 65): # Vérifie si un retour circulaire est requis.
                codeCar = codeCar+26-decalage
            else:
                codeCar = codeCar-decalage
        if( (codeCar >= 97) & (codeCar <= 122) ): # Vérifie que le caractère soir dans l'alphabet MINUSCULE.
            if (codeCar-decalage < 97): # Vérifie si un retour circulaire est requis.
                codeCar = codeCar + 26 - decalage
            else:
                codeCar = codeCar - decalage
        
        liste[indice] = chr(codeCar)
        indice = indice + 1
    chainedeChiffree = "".join(liste) # Concatène tous les caractères de la liste pour former une chaine (ex: ['H','e','y'] devient la chaine "Hey")
    return chainedeChiffree
 #=================================================